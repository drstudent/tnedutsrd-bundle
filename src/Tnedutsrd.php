<?php

namespace TnedutsrdBundle;

class Tnedutsrd{

  private $wordProvider;

  private $unicornsAreReal;
  private $minSunshine;

  public function __construct(WordProviderInterface $wordProvider, bool $unicornsAreReal = true, $minSunshine = 7)
  {
//    var_dump($wordProvider);
//    die();
//    $wordProvider = new TnedutsrProvider();
    $this->wordProvider = $wordProvider;

    $this->unicornsAreReal = $unicornsAreReal;
    $this->minSunshine = $minSunshine;

//    $x = $this->wordProvider->getWordList();
//    var_dump($x);
//    die();
  }

  function getWords (){
    return $this->wordProvider->getWordList();
  }
}
