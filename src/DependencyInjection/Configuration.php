<?php

namespace TnedutsrdBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\NodeInterface;
use TnedutsrdBundle\TnedutsrdDummyAInterface;
use TnedutsrdBundle\TnedutsrdDummyBInterface;
use TnedutsrdBundle\TnedutsrdProvider;

class Configuration implements ConfigurationInterface
{
  public function getConfigTreeBuilder()
  {
    $treeBuilder = new TreeBuilder('tnedutsr');
    $rootNode = $treeBuilder->getRootNode();

    $rootNode
      ->children()
      ->booleanNode('unicorns_are_real')->defaultTrue()->info('Whether or not you believe in unicorns')->end()
      ->integerNode('min_sunshine')->defaultValue(3)->info('How much do you like sunshine?')->end()
      ->scalarNode('word_provider')->defaultNull()->end()
      ->end();

    $rootNode
      ->children()
      ->scalarNode('provider')->defaultNull()->end()
      ->scalarNode('account_key')->defaultNull()->end()
      ->end();

    $entityInterfaces = [
      TnedutsrdDummyAInterface::class,
      TnedutsrdDummyBInterface::class
    ];

    $rootNode
      ->children()
        ->arrayNode('doctrine_account_entities')
          ->arrayPrototype()
            ->children()
              ->scalarNode('entity')
                ->isRequired()
                ->cannotBeEmpty()
                ->validate()
                  ->ifTrue(
                    function($class) use ($entityInterfaces){
                      $interfaces = class_implements($class);
                      return !$interfaces || !array_intersect($entityInterfaces, $interfaces);
                    }
                  )
                  ->thenInvalid(
                    'Entity class must implement at least one of interfaces: ' .
                    implode(', ', $entityInterfaces)
                  )
                ->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end();

    return $treeBuilder;
  }
}
