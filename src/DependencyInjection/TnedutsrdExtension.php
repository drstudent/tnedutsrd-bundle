<?php

namespace TnedutsrdBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\FileLocator;

class TnedutsrdExtension extends Extension
{
  //it is default to it
//  public function getAlias()
//  {
//    return 'tnedutsrd';
//  }

  public function load(array $configs, ContainerBuilder $container)
  {
    $configuration = $this->getConfiguration($configs, $container);
    $config = $this->processConfiguration($configuration, $configs);

    $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
    $loader->load('services.xml');

    $definition = $container->getDefinition('tnedutsrd.tnedutsrd');

    if (null !== $config['word_provider']) {
//      $definition->setArgument(0, new Reference($config['word_provider']));
      //more advanced
      $container->setAlias('tnedutsrd.tnedutsrd_provider', $config['word_provider']);
    }

    $definition->setArgument(1, $config['unicorns_are_real']);
    $definition->setArgument(2, $config['min_sunshine']);

    $definition = $container->getDefinition('tnedutsrd.tnedutsrd_authenticator');
    if (null !== $config['provider']) {
      $definition->setArgument(0, new Reference($config['provider']));
    }
    if (null !== $config['account_key']) {
      $definition->setArgument(1, $config['account_key']);
    }

    $definition = $container->getDefinition('tnedutsrd.dummy_event_listener');

    $definition->addTag('doctrine.orm.entity_listener', [
      'event'          => 'postUpdate',
      'entity'         => 'App\Entity\Manager'
    ]);

    $doctrineAccountEntities = $config['doctrine_account_entities'];
    foreach ($doctrineAccountEntities as $doctrineAccountEntity){
//      var_dump($doctrineAccountEntity);
    }

  }
}
