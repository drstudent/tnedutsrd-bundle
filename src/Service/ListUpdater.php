<?php

namespace TnedutsrdBundle\Service;

class ListUpdater {
  private static $instance = null;

  private $data = [];

  private function __construct() {
  }
  private function __clone() {
  }

  static public function getInstance() {
    if(is_null(self::$instance)) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  public function add($el) {
    $this->data[] = $el;
  }

  public function get() {
    return $this->data;
  }
}