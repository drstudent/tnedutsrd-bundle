<?php

namespace TnedutsrdBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SyncCredentialsCommand extends Command
{
  protected static $defaultName = 'tnedutsrd:sync-credentials';


  public function __construct()
  {
    parent::__construct();
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {


    $output->writeln('OK.');
    return Command::SUCCESS;
  }
}
