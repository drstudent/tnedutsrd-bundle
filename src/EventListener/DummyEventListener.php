<?php

namespace TnedutsrdBundle\EventListener;

use App\Entity\Manager;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use TnedutsrdBundle\TnedutsrdProvider;
use TnedutsrdBundle\TnedutsrdDummyInterface;
use TnedutsrdBundle\TnedutsrdDummyAInterface;
use TnedutsrdBundle\TnedutsrdDummyBInterface;

use TnedutsrdBundle\Service\ListUpdater;

class DummyEventListener
{
  private $logger;

  public function __construct(LoggerInterface $logger = null)
  {
    $this->logger = $logger;
  }

  public function postPersist(Manager $manager, LifecycleEventArgs $event): void
  {
    $this->logger->info("Dummy2EventListenerFired", [
      'e' => 'postPersist'
    ]);
  }

  public function postUpdate(Manager $manager, LifecycleEventArgs $event): void
  {
//    $interfaces = class_implements('TnedutsrdBundle\TnedutsrdProvider');
//    $this->logger->info("Dummy2EventListenerFired", [
//      'e' => 'yy',
//      'x' => TnedutsrdProvider::xxxx(),
//      'y' => $interfaces,
//      'i' => $interfaces && in_array(TnedutsrdDummyInterface::class, $interfaces)
//    ]);
    $data = [];
    if ($manager instanceof TnedutsrdDummyAInterface) {
      $value = $manager->a();
      if ($value !== ''){
        $data['a'] = $value;
      }
    }
    if ($manager instanceof TnedutsrdDummyBInterface) {
      $value = $manager->b();
      if ($value !== ''){
        $data['b'] = $value;
      }
    }
    if ($data){
      ListUpdater::getInstance()->add($data);
    }
    $this->logger->info("Dummy2EventListenerFired", [
      'e' => 'postUpdate',
      'data' => $data,
      'a' => $event
    ]);

  }

  public function postRemove(Manager $manager, LifecycleEventArgs $event): void
  {
    $this->logger->info("Dummy2EventListenerFired", [
      'e' => 'postRemove'
    ]);


  }
}

