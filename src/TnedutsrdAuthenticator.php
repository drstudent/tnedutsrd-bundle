<?php


namespace TnedutsrdBundle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\PreAuthenticatedUserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;

/**
 * This authenticator authenticates a remote user.
 *
 * @author Wouter de Jong <wouter@wouterj.nl>
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Maxime Douailin <maxime.douailin@gmail.com>
 *
 * @final
 *
 * @internal in Symfony 5.1
 */
class TnedutsrdAuthenticator  implements AuthenticatorInterface
{

  private $userProvider;

  private $accountKey;

  public function __construct(UserProviderInterface $userProvider, string $accountKey = 'REMOTE_USER')
  {
    $this->userProvider = $userProvider;
    $this->accountKey = $accountKey;
  }

  public function supports(Request $request): ?bool
  {
    return $request->server->has($this->accountKey);
  }

  /**
   * Create a passport for the current request.
   *
   * You may throw any AuthenticationException in this method in case of error (e.g.
   * a UserNotFoundException when the user cannot be found).
   *
   * @throws AuthenticationException
   */
  public function authenticate(Request $request): PassportInterface
  {
    $accountName = $request->server->get($this->accountKey);

    // @deprecated since Symfony 5.3, change to $this->userProvider->loadUserByIdentifier() in 6.0
    $method = 'loadUserByIdentifier';
    if (!method_exists($this->userProvider, 'loadUserByIdentifier')) {
      trigger_deprecation('symfony/security-core', '5.3', 'Not implementing method "loadUserByIdentifier()" in user provider "%s" is deprecated. This method will replace "loadUserByUsername()" in Symfony 6.0.', get_debug_type($this->userProvider));
      $method = 'loadUserByUsername';
    }

    return new SelfValidatingPassport(
      new UserBadge($accountName, [$this->userProvider, $method]),
      [new PreAuthenticatedUserBadge()]
    );
  }

  public function createAuthenticatedToken(PassportInterface $passport, string $firewallName): TokenInterface
  {
    return new PreAuthenticatedToken($passport->getUser(), null, $firewallName, $passport->getUser()->getRoles());
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
  {
    return null;
  }

  public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
  {
    return null;
  }
}
