<?php

namespace TnedutsrdBundle;

class TnedutsrdProvider implements WordProviderInterface, TnedutsrdDummyInterface
{
  public function getWordList(): array
  {
    return [
      'adorable',
      'active',
      'admire',
      'adventurous',
    ];
  }

  public static function xxxx(): string{
    return 'xxxx...xxx';
  }

  public function a(): string{
    return '';
  }

  public function b(): string{
    return '';
  }
}
