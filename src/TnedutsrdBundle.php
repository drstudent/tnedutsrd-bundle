<?php

namespace TnedutsrdBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
//use TnedutsrdBundle\DependencyInjection\TnedutsrdExtension;

class TnedutsrdBundle extends Bundle{
  //we dont need it as our alias is standard and dont need to be renamed  as here
  //https://symfonycasts.com/screencast/symfony-bundle/custom-extension-alias#play
//  /**
//   * Overridden to allow for the custom extension alias.
//   */
//  public function getContainerExtension()
//  {
//    if (null === $this->extension) {
//      $this->extension = new TnedutsrdExtension();
//    }
//    return $this->extension;
//  }
}
