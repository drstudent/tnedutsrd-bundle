<?php

namespace TnedutsrdBundle;

interface TnedutsrdDummyBInterface extends TnedutsrdDummyBaseInterface
{
  public function b(): string;
}
